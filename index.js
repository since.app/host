// Package imports
const files = require('fastify-static')
const path = require('path').resolve
const server = require('since.fast')

// Application imports
const env = require('./.env')

// Adding fastify-static config
env.root = path(__dirname, 'public')

// Run server
server([files], env)
